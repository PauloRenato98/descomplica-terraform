# Terraform
Aqui foi criado um cluster terraform na aws e feito uma pipeline de init, plan e apply para aplicar as alterações na infra.

# INIT
![image](docs/pipeinit.PNG)

# PLAN
![image](docs/pipeplan.PNG)

# APPLY
![image](docs/pipeapply.PNG)

# Cluster
![image](docs/cluster.PNG)

# S3
Foi criado um bucket s3 para salvar o state do terraform para não ser perdido, toda vez que tiver um novo estado ele atualiza e manda pro bucket.

![image](docs/state.png)

